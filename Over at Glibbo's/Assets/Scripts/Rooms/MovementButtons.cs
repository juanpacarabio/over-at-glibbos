using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementButtons : MonoBehaviour
{
    [SerializeField] private GameObject RoomUp, RoomDown, RoomLeft, RoomRight, ThisRoom;

    public void MoveUp()
    {
        RoomUp.SetActive(true);
        ThisRoom.SetActive(false);
    }

    public void MoveDown()
    {
        RoomDown.SetActive(true);
        ThisRoom.SetActive(false);
    }

    public void MoveLeft() 
    { 
        RoomLeft.SetActive(true);
        ThisRoom.SetActive(false);
    }

    public void MoveRight() 
    { 
        RoomRight.SetActive(true);
        ThisRoom.SetActive(false);
    }

}
